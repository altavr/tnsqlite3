

    use tnsqlite3::{self,*};
//    pub use sqlite3_sys1::*;
    use std::ffi::{CStr, CString};
    use std::mem;
    use std::os::raw::*;
    use std::ptr;

    #[derive(Debug, PartialEq, Mapping, Values, Insert)]
    #[table="task"]
    struct TaskRow {
        id: Option<i64>,
        name: Option<String>,
    }

    #[derive(Debug, Mapping, Values, Insert)]
    #[table="stuff"]
    struct Stuff {
        id: i64,
        name: String,
        name1: Option<String>,
        val: Option<i64>,
    }

    #[derive(Debug, PartialEq, Mapping, Values, Insert)]
    #[table="chair"]
    struct Chair {
        id: i64,
        fid: i64,
    }

    #[derive(Debug, PartialEq, Mapping, Values, Insert)]
    #[table="bindata"]
    struct Bin {
        data: Option<Vec<u8>>
    }

fn row_count(table: &str, conn: & Connection ) -> i64 {
    let stmt = format!("SELECT COUNT(*) FROM {}", table);
//    conn.execute(&stmt, &[]).unwrap();
    let p_stmt = conn.prepare(&stmt, &[]).unwrap();
    p_stmt.query(select_tuple!(i64)).unwrap().next().unwrap().unwrap()

}

fn create_tables( conn: &mut Connection) {
    let table_task =
    "CREATE TABLE task (
    id INTEGER not null,
    name  TEXT )";

    let table_stuff =
    "CREATE TABLE stuff (
    id INTEGER PRIMARY KEY,
    name  TEXT NOT NULL,
    name1  TEXT,
    val INTEGER UNIQUE)";

    let bindata =
    "CREATE TABLE bindata (
    data    BINARY NOT NULL)";

    for i in &[table_task, table_stuff, bindata] {
        conn.execute(i, &[]).unwrap();
    }

}


#[test]
fn   zeroblob_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    conn.register_cdatetime_func().expect("register_cdatetime_func");
    create_tables(&mut conn);
    let stmt = "INSERT INTO bindata (data) VALUES (  zeroblob(5)  )";
    conn.execute(stmt, &[]).expect("conn.execute");
    let stmt = format!("SELECT * FROM bindata");
    let  p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let rows =   p_stmt.query(Bin::mapping).unwrap().fallible_collect().unwrap();
    assert!(rows.len() == 1);
    assert!(rows[0].data.as_ref().unwrap().len() == 5);
    println!("{:?}", &rows);
}


#[test]
fn bin_or_vec_null_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    conn.register_cdatetime_func().expect("register_cdatetime_func");
    create_tables(&mut conn);
    let stmt = "INSERT INTO bindata (data) VALUES ( ? )";

    let stmt = format!("SELECT bin_or_vec(data, 5) FROM bindata");
    let  p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let rows =   p_stmt.query(Bin::mapping).unwrap().fallible_collect().unwrap();
    assert!(rows.len() == 1);
    assert!(rows[0].data.is_none());
    println!("{:?}", &rows);
}



#[test]
fn bin_or_vec_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    conn.register_cdatetime_func().expect("register_cdatetime_func");
    create_tables(&mut conn);
    let stmt = "INSERT INTO bindata (data) VALUES ( ? )";
    let blob0 =  Bin { data: Some( vec![1, 1, 0, 0 ,1]) };
    blob0.insert(&mut conn);
    let blob1  = Bin { data: Some( vec![1, 0, 1, 1 ,1]) };
    blob1.insert(&mut conn);

    let stmt = format!("SELECT bin_or_vec(data, 5) FROM bindata");
    let  p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let rows =   p_stmt.query(Bin::mapping).unwrap().fallible_collect().unwrap();
    assert!(rows.len() == 1);
    assert_eq!( &vec![  Bin{data: Some (vec![1, 1, 1, 1 ,1] )}  ], &rows  );
    println!("{:?}", &rows);

}

#[test]
fn cdatetime_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    conn.register_cdatetime_func().expect("register_cdatetime_func");
    create_tables(&mut conn);
    let stmt = "INSERT INTO task (id, name) VALUES ( cdatetime(), 'space' )";
    conn.execute(&stmt, &[]).expect("conn.execute");

    let stmt = format!("SELECT * FROM task");
    let  p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let rows =   p_stmt.query(TaskRow::mapping).unwrap().fallible_collect().unwrap();
    println!("{:?}", &rows);
//    assert_eq!(&rows, &rows[0]);

}
#[test]
fn fmtdatetime_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    conn.register_cdatetime_func().expect("register_cdatetime_func");
    create_tables(&mut conn);
    let stmt = "INSERT INTO task (id, name) VALUES (1, fmtdatetime( 345253 ))";
    conn.execute(&stmt, &[]).expect("conn.execute");

    let stmt = format!("SELECT * FROM task");
    let p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let rows =   p_stmt.query(TaskRow::mapping).unwrap().fallible_collect().unwrap();
    println!("{:?}", &rows);
//    assert_eq!(&rows, &rows[0]);

}

#[test]
fn bindata_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    create_tables(&mut conn);
    let bind = Bin{data: Some(vec![1,2,3])};
    bind.insert(&mut conn).unwrap();
    let stmt = format!("SELECT data FROM bindata");
   let mut p_stmt =  conn.prepare(&stmt, &[]).unwrap();
    let binvec =    p_stmt.query(Bin::mapping).unwrap().fallible_collect().unwrap();
    println!("{:?}", &binvec);
    assert_eq!(&bind, &binvec[0]);

}


#[test]
fn insert_many_test() {
    let mut conn = Connection::new(":memory:").unwrap();
    create_tables(&mut conn);

    let mut p_stmt = conn.prepare("INSERT INTO task (id, name) VALUES (?,?)",
                      params!( 2, "ggg" )  ).unwrap();
    p_stmt.insert(params!( 3, "hhh" ) ).unwrap();
    assert_eq!(row_count("task", & conn), 2);
}


#[test]
fn insert_trait_test() {
    let mut db = Connection::new(":memory:").unwrap();
    create_tables(&mut db);

    let task = TaskRow { id: Some(2), name: Some( "ggg".to_string())};
    task.insert(&db).unwrap();
    assert_eq!(row_count("task", & db), 1);
}

#[test]
fn transactions_test() {
    let mut db = Connection::new(":memory:").unwrap();
    create_tables(&mut db);
    {
    let mut ta = db.transaction().expect("transaction");
    db.execute("INSERT INTO stuff (name, name1, val) VALUES (?,?,?)",
            params!("a", "b", 1)).expect("execute");
        row_count("stuff", &db);
    db.execute("INSERT INTO stuff ( name, name1, val) VALUES (?,?,?)",
            params!("a", "b", 2)).expect("execute");
    ta.rollback().expect("rollback");
    }
    assert_eq!(row_count("stuff", &mut db), 0);
}

#[test]
fn transactions_commit_test() {
    let mut db = Connection::new(":memory:").unwrap();
    create_tables(&mut db);
    {
    let mut ta = db.transaction().expect("transaction");
    db.execute("INSERT INTO stuff (name, name1, val) VALUES (?,?,?)",
            params!("a", "b", 1)).expect("execute");
            row_count("stuff", &db);
    db.execute("INSERT INTO stuff ( name, name1, val) VALUES (?,?,?)",
            params!("a", "b", 2)).expect("execute");
    }
    assert_eq!(row_count("stuff", &mut db), 2);
}
