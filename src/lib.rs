#![warn(clippy::all)]

#[macro_use]


extern crate bitflags;

mod raw_items;
use std::ffi::NulError;
use std::string::FromUtf8Error;
use std::{error, fmt, str};
use std::cell::RefCell;
use std::ptr;
use std::os::raw::{c_void, c_int};
pub use tnsqlite3_proc::{Insert, Mapping, Mapped, Values, Identify};
pub use raw_items::{ FunctionFlag};
pub use sqlite3_sys1 as ffi;

pub use crate::raw_items::OpenFlags;
use crate::raw_items::{RawConnection, RawStatement, Sqlite3Type, cdatetime, fmtdatetime,
 bin_or_vec_step, bin_or_vec_final, zeroblob};
// extern crate sqlite3_sys1;

//const STMT_COUNT: usize = 4;

#[derive(Debug, Clone, PartialEq)]
pub enum IRR {
    ParamMismatch,
    PathNotValid,
    TransVal,
    NoCurrentStmt,
    ColumnOutOfRange,
    UnexpectNull(i32),
    GetValue((i32, ffi::Error)),
    ConversionFailed
}

#[derive(Debug, PartialEq, Clone)]
pub struct InnerError {
    kind: IRR,
}

impl InnerError {
    pub fn new(kind: IRR) -> InnerError {
        InnerError { kind }
    }
}

#[derive(Debug, Clone)]
pub enum Error {
    Core(ffi::Error),
    InnerError(InnerError),
    FromUtf8,
    Utf8(str::Utf8Error),
    NullError(NulError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {}

#[derive(Debug)]
pub enum ErrorKind {
    ParamMismath,
    PathNotValid,
    TransVal,
    NoCurrentStmt,

    Core(ffi::Error),
    FromUtf8(FromUtf8Error),
    Utf8(str::Utf8Error),
    NullError(NulError),
}

macro_rules! impl_from_error {
    ($t:ty, $var:ident) => {
        impl From<$t> for Error {
            fn from(v: $t) -> Error {
                Error::$var(v)
            }
        }
    };
}

impl_from_error!(NulError, NullError);

impl_from_error!(str::Utf8Error, Utf8);
impl_from_error!(ffi::Error, Core);
impl_from_error!(InnerError, InnerError);

macro_rules! impl_from_error_pure {
    ($t:ty, $var:ident) => {
        impl From<$t> for Error {
            fn from(_: $t) -> Error {
                Error::$var
            }
        }
    };
}
impl_from_error_pure!(FromUtf8Error, FromUtf8);

#[derive(Debug, Clone, PartialEq)]
pub enum Sqlite3Ref<'a> {
    NULL,
    INTEGER(i64),
    DOUBLE(f64),
    BLOB(&'a [u8]),
    TEXT(&'a str),
}

macro_rules! impl_from_for_Sqlite3Ref {
    ($t:ty, $var:ident) => {
        impl<'a> From<$t> for Sqlite3Ref<'a> {
            fn from(v: $t) -> Sqlite3Ref<'a> {
                Sqlite3Ref::$var(v)
            }
        }
    };
}

macro_rules! impl_from_for_int_Sqlite3Ref {
    ($t:ty, $var:ident) => {
        impl<'a> From<$t> for Sqlite3Ref<'a> {
            fn from(v: $t) -> Sqlite3Ref<'a> {
                Sqlite3Ref::$var(v as i64)
            }
        }
    };
}

impl_from_for_int_Sqlite3Ref!(i32, INTEGER);
impl_from_for_Sqlite3Ref!(i64, INTEGER);
impl_from_for_Sqlite3Ref!(f64, DOUBLE);
impl_from_for_Sqlite3Ref!(&'a str, TEXT);
impl_from_for_Sqlite3Ref!(&'a String, TEXT);
impl_from_for_Sqlite3Ref!(&'a [u8], BLOB);
impl_from_for_Sqlite3Ref!(&'a Vec<u8>, BLOB);

macro_rules! impl_from_ref_for_Sqlite3Ref {
    ($t:ty, $var:ident) => {
        impl<'a> From<$t> for Sqlite3Ref<'a> {
            fn from(v: $t) -> Sqlite3Ref<'a> {
                Sqlite3Ref::$var(*v)
            }
        }
    };
}

impl_from_ref_for_Sqlite3Ref!(&'a i64, INTEGER);
impl_from_ref_for_Sqlite3Ref!(&'a f64, DOUBLE);

macro_rules! impl_from_ref_option_for_Sqlite3Ref {
    ($t:ty,) => {
        impl<'a> From<&'a Option<$t>> for Sqlite3Ref<'a> {
            fn from(v: &'a Option<$t>) -> Sqlite3Ref<'a> {
                match v {
                    Some(x) => x.into(),
                    None => Sqlite3Ref::NULL,
                }
            }
        }
    };
}

impl_from_ref_option_for_Sqlite3Ref!(Vec<u8>,);
impl_from_ref_option_for_Sqlite3Ref!(String,);
impl_from_ref_option_for_Sqlite3Ref!(i64,);



#[derive(Debug)]
pub enum Sqlite3Value {
    NULL,
    INTEGER(i64),
    DOUBLE(f64),
    BLOB(Vec<u8>),
    TEXT(String),
}

impl Sqlite3Value {
    pub fn to_sqlite3ref(&self) -> Sqlite3Ref {
        use Sqlite3Value::*;
        match self {
            NULL => Sqlite3Ref::NULL,
            INTEGER(v) => Sqlite3Ref::INTEGER(*v),
            DOUBLE(v) => Sqlite3Ref::DOUBLE(*v),
            BLOB(v) => Sqlite3Ref::BLOB(v.as_slice()),
            TEXT(v) => Sqlite3Ref::TEXT(v.as_str()),
        }
    }
}

macro_rules! impl_from_for_Sqlite3Val {
    ($t:ty, $var:ident) => {
        impl<'a> From<$t> for Sqlite3Value {
            fn from(v: $t) -> Sqlite3Value {
                Sqlite3Value::$var(v)
            }
        }
    };
}

impl_from_for_Sqlite3Val!(i64, INTEGER);
impl_from_for_Sqlite3Val!(f64, DOUBLE);
impl_from_for_Sqlite3Val!(String, TEXT);
impl_from_for_Sqlite3Val!(Vec<u8>, BLOB);

macro_rules! impl_from_Sqlite3Val {
    ($t:ty, $var:ident) => {
        impl From<Sqlite3Value> for $t {
            fn from(v: Sqlite3Value) -> $t {
                match v {
                    Sqlite3Value::$var(x) => x,
                    _ => panic!("Type not match variant. Expect {}, got {:?}",
                         stringify!($t), v),
                }
            }
        }
    };
}

impl_from_Sqlite3Val!(String, TEXT);
impl_from_Sqlite3Val!(i64, INTEGER);
impl_from_Sqlite3Val!(f64, DOUBLE);
impl_from_Sqlite3Val!(Vec<u8>, BLOB);


pub trait Mapping {
    type Target;
    fn mapping(row: &mut Row) -> Result<Self::Target, Error>;
}

pub trait Mapped<T> {
    fn mapped(row: &mut Row, column: i32) -> Result<T, Error>;
}


impl<T> Mapping for T  where T: Mapped<T> {
    type Target = T;
    fn mapping(row: &mut Row) -> Result<Self::Target, Error> {
        T::mapped(row, 0)
}

}


macro_rules! impl_mapped {
    ($target:ty) => {
        impl Mapped<$target> for $target {
            fn mapped(row: &mut Row, column: i32) -> Result<Self, Error> {
                row.get(column)?
                    .ok_or_else(|| Error::InnerError(
                    InnerError::new(IRR::UnexpectNull(column))))
            }
        }
    };
}

macro_rules! impl_mapped_option {
    ($target:ty) => {
        impl Mapped<Option<$target>> for Option<$target> {
            fn mapped(row: &mut Row, column: i32) -> Result<Self, Error> {
               Ok( row.get(column)?)
            }
        }
    };
}


impl_mapped!(i64);
impl_mapped_option!(i64);
impl_mapped!(f64);
impl_mapped_option!(f64);
impl_mapped!(Vec<u8>);
impl_mapped_option!(Vec<u8>);
impl_mapped!(String);
impl_mapped_option!(String);




//#[macro_export]
//macro_rules! mapped_row {
//    ($count:expr, [$head:ty, $($tail:ty),*]) => {
//                                <$head as Mapped<$head>>::mapped(row, $count)?,
//                               mapped_row!($count + 1, [ $($tail,)*])
//        };
//    ($count:expr, [ ]) => { };
//}

//#[macro_export]
//macro_rules! type_tuple {
//    ($($t:ty),+) => {  ( $($t),+ )     };
//}

#[macro_export]
macro_rules! select_tuple {

//    ( $($t:ty),+) => {
//        |row: &mut tnsqlite3::Row| -> std::result::Result<type_tuple!($($t),+), tnsqlite3::Error> {
//          Ok ( ( mapped_row!(0, [$($t),+]), ) )
//        }
//    };



    ( $t0:ty) => {
        |row: &mut tnsqlite3::Row| -> std::result::Result<$t0, tnsqlite3::Error> {
            Ok(<$t0 as Mapped<$t0>>::mapped(row, 0)?)
        }
    };

    ( $t0:ty, $t1:ty ) => {
        |row: &mut tnsqlite3::Row| -> std::result::Result<($t0, $t1), tnsqlite3::Error> {
            Ok((
                <$t0 as Mapped<$t0>>::mapped(row, 0)?,
                <$t1 as Mapped<$t1>>::mapped(row, 1)?,
            ))
        }
    };

    ( $t0:ty, $t1:ty, $t2:ty ) => {
        |row: &mut tnsqlite3::Row| -> std::result::Result<($t0, $t1, $t2), tnsqlite3::Error> {
            Ok((
                <$t0 as Mapped<$t0>>::mapped(row, 0)?,
                <$t1 as Mapped<$t1>>::mapped(row, 1)?,
                <$t0 as Mapped<$t2>>::mapped(row, 2)?,
            ))
        }
    };
    ( $t0:ty, $t1:ty, $t2:ty, $t3:ty ) => {
        |row: &mut tnsqlite3::Row| -> std::result::Result<($t0, $t1, $t2, $t3), tnsqlite3::Error> {
            Ok((
                <$t0 as Mapped<$t0>>::mapped(row, 0)?,
                <$t1 as Mapped<$t1>>::mapped(row, 1)?,
                <$t2 as Mapped<$t2>>::mapped(row, 2)?,
                <$t3 as Mapped<$t3>>::mapped(row, 3)?,
            ))
        }
    };
}



pub trait Values {
    fn values(&self) -> Vec<Sqlite3Ref>;
}

pub trait Insert: Values {
    fn stmt() -> &'static str;
    fn insert(&self, conn: & Connection) -> Result<i64, Error> {
        conn.execute(Self::stmt(), &self.values())?;
        Ok(conn.last_insert_rowid())
    }
}

pub trait Identify {
fn identify(&self) -> i64;

}

pub struct Transaction<'c> {
    conn: &'c Connection,
}

impl<'c> Transaction<'c> {

    pub fn commit(& self) -> Result<(), Error> {
        self.conn.execute("COMMIT", &[])
    }

    pub fn rollback(& self) -> Result<(), Error> {
        self.conn.execute("ROLLBACK", &[])
    }


}

impl<'c> Drop for Transaction<'c> {

    fn drop(&mut self) {
        if !self.conn.autocommit() {
             let _ =  self.commit().or_else(|_| self.rollback() );
      }
    }
}

unsafe impl Send for Connection {}

#[derive(Debug)]
pub struct Connection {
    conn: RefCell<RawConnection>,
}

impl Connection {
    pub fn new(uri: &str) -> Result<Connection, Error> {
        Connection::with_flags(
            uri,
            OpenFlags::READWRITE | OpenFlags::CREATE | OpenFlags::URI,
        )
    }

    pub fn with_flags(uri: &str, flags: OpenFlags) -> Result<Connection, Error> {
        let r_conn = RawConnection::new(uri, flags)?;
        Ok(Connection {
            conn: RefCell::new(r_conn),
        })
    }

    fn get_raw(&self)  -> & RefCell<RawConnection> {
        &self.conn

    }

//    pub fn handler(&self) -> Sqlite3Handler {
//        self.conn.borrow().handler() }

    pub fn transaction(& self) -> Result<Transaction<'_>, Error> {
        self.execute("BEGIN TRANSACTION", &[])?;
       Ok( Transaction {conn:  self} )
    }


    fn execute_(&self, sql_stmt: &str, params: &[Sqlite3Ref]) -> Result<Statement, Error> {
    println!("{} : {:?}", sql_stmt, params);
        let raw_stmt = self.conn.borrow_mut().prepare(sql_stmt)?;
        let mut stmt = Statement::new(raw_stmt);
        stmt.bind_params(params)?;
        Ok(stmt)
    }

   pub fn execute(&self, sql_stmt: &str, params: &[Sqlite3Ref]) -> Result<(), Error> {

        let mut stmt = self.execute_(sql_stmt, params)?;
        stmt.step()?;
        Ok(())
    }

    pub fn prepare(&self, sql_stmt: &str, params: &[Sqlite3Ref]) -> Result<PStatement, Error> {

    let mut stmt = self.execute_(sql_stmt, params)?;
    let rc = stmt.step()?;
    Ok (PStatement { conn: self, stmt, status_code: Ok(rc) }   )

    }



    pub fn last_insert_rowid(&self) -> i64 {
        self.conn.borrow().last_insert_rowid()
    }

    fn autocommit(&self) -> bool {
        self.conn.borrow_mut().autocommit()
    }

    pub fn register_cdatetime_func(&mut self) -> Result<(), Error>  {
        let conn = self.conn.get_mut();
        conn.create_aggregate_function("bin_or_vec", 2, bin_or_vec_step, bin_or_vec_final,
          FunctionFlag::NonFlag, ptr::null_mut()  )?;
          conn.create_function( "zeroblob", 1, zeroblob,
              FunctionFlag::Deterministic, ptr::null_mut())?;
        conn.create_function( "fmtdatetime", 1, fmtdatetime,
            FunctionFlag::Deterministic, ptr::null_mut())?;
        Ok(conn.create_function(
            "cdatetime", 0, cdatetime, FunctionFlag::NonFlag, ptr::null_mut())?)
    }

    pub fn create_function(
        &mut self,
        name: &str,
        nargs: i32,
        func: extern "C" fn(*mut ffi::sqlite3_context, c_int, *mut *mut ffi::sqlite3_value),
        flag: FunctionFlag,
        app: *mut c_void
    ) ->  Result<(), Error>  {
       Ok( self.conn.borrow_mut().create_function( name, nargs, func, flag, app )?)

     }

}


pub struct PStatement<'c> {
    conn: &'c Connection,
    stmt: Statement,
    status_code: Result<ffi::ResultCode, Error>,
}

impl<'c> PStatement<'c> {

    pub fn query<C, T, E>(
        self,
        mapper: C,
    ) -> Result<QueryMapIter<'c, C>, Error>
    where
        C: Fn(&mut Row) -> Result<T, E>,
        E: std::error::Error + From<Error>,
    {

        let row = Row::new(self.conn, self.stmt);
        Ok(QueryMapIter {
            row,
            factory: mapper,
            status_code: self.status_code,
            init_row: true,
        })
    }

    pub fn query_row(self) -> Result<QueryIter<'c>, Error> {

        Ok(QueryIter {
            conn: self.conn,
            status_code: self.status_code,
            stmt: self.stmt,
            init_row: true,
        })
    }

    pub fn insert(&mut  self, params: &[Sqlite3Ref]) ->
        Result<(), Error> {
    println!("{:?}",  params);
        self.stmt.reset()?;
        self.stmt.bind_params(params)?;
        self.stmt.step()?;
        Ok(())
    }

}


#[derive(Debug)]
pub struct Statement {
    r_stmt: RawStatement,
    status: Option<ffi::ResultCode>,
}

impl Statement {
    fn new(r_stmt: RawStatement) -> Statement {
        Statement {
            r_stmt,
            status: None,
        }
    }

    fn get<T>(&self, i_col: i32, conn: &Connection) -> Result<Option<T>, Error>
    where
        T: From<Sqlite3Value>,
    {
        let r = self.get_(i_col, conn);
        match r {
            Ok(v) => Ok(v),
            Err(Error::Core(e)) => Err(Error::InnerError(InnerError::new(IRR::GetValue((
                i_col, e,
            ))))),
            Err(e) => Err(e),
        }
    }
    fn get_<T>(&self, i_col: i32, conn: &Connection) -> Result<Option<T>, Error>
    where
        T: From<Sqlite3Value>,
    {
        use Sqlite3Type::*;
        let r_stmt = &self.r_stmt;

        let val = match r_stmt.column_type(i_col)? {
            INTEGER => Sqlite3Value::INTEGER(r_stmt.column_int64(i_col)?),
            DOUBLE => Sqlite3Value::DOUBLE(r_stmt.column_double(i_col)?),
            BLOB => Sqlite3Value::BLOB(match r_stmt.column_blob(i_col, conn.get_raw())? {
                Some(x) => x,
                None => return Ok(None),
            }),
            TEXT => Sqlite3Value::TEXT(match r_stmt.column_text(i_col, &conn.get_raw())? {
                Some(x) => x,
                None => return Ok(None),
            }),
            NULL => {
                return Ok(None);
            }
        };
        Ok(Some(From::from(val)))
    }

    fn bind_params(&mut self, params: &[Sqlite3Ref]) -> Result<(), Error> {
        use Sqlite3Ref::*;
        for (i, param) in params.iter().enumerate() {
            let i = i as i32 + 1;
            match *param {
                INTEGER(x) => self.r_stmt.bind_int64(i, x)?,
                DOUBLE(x) => self.r_stmt.bind_double(i, x)?,
                BLOB(x) => self.r_stmt.bind_blob(i, x)?,
                TEXT(x) => self.r_stmt.bind_text(i, x)?,
                NULL => self.r_stmt.bind_null(i)?,
            }
        }
        Ok(())
    }

    fn step(&mut self) -> Result<ffi::ResultCode, Error> {
        self.r_stmt.step().map_err(|e| Error::Core(e))
    }



    fn reset(&mut self) -> Result<(), Error> {
        self.r_stmt.reset().map_err(|e| Error::Core(e))
    }
    fn column_count(&self) -> i32  {
        self.r_stmt.column_count()
}

}

pub struct Row<'b> {
    conn: &'b Connection,
    stmt: Statement,
}

impl<'b> Row<'b> {
    fn new(conn: &'b Connection, stmt: Statement) -> Self {
        Row { conn, stmt }
    }

    pub fn get<T>(&mut self, i_col: i32) -> Result<Option<T>, Error>
    where
        T: From<Sqlite3Value>,
    {
        self.stmt.get(i_col, &self.conn)
    }

    pub fn count(&self) -> i32 {
        self.stmt.column_count()
    }
}



pub struct QueryIter<'b> {
    conn: &'b  Connection,
    status_code: Result<ffi::ResultCode, Error>,
    stmt: Statement,
    init_row: bool,
}

impl<'b> QueryIter<'b> {
    pub fn get<T>(&mut self, i_col: i32) -> Result<Option<T>, Error>
    where
        T: From<Sqlite3Value>,
    {
        self.stmt.get(i_col, &self.conn)
    }

    pub fn count(&self) -> i32 {
        self.stmt.column_count()
    }

    pub fn next_row(&mut self) -> Option<Result<(), Error>> {
        let stmt = &mut self.stmt;
        if self.init_row {
            match &self.status_code {
                Ok(c) if *c == ffi::ResultCode::Row => self.init_row = false,
                _ => return None,
            }
            self.init_row = false;
            stmt.status = Some(ffi::ResultCode::Done);
        } else {
            self.status_code = stmt.step()
        };
        match &self.status_code {
            Ok(c) if *c == ffi::ResultCode::Row => Some(Ok(())),
            Ok(_) => {
                self.status_code = Ok(ffi::ResultCode::Done);
                self.init_row = true;
                None
            }
            Err(e) =>  Some(Err(e.clone())),
        }
    }
}


pub struct QueryMapIter<'b, C> {
    row: Row<'b>,
    factory: C,
    status_code: Result<ffi::ResultCode, Error>,
    init_row: bool,
}

impl<'b, C, T, E> Iterator for QueryMapIter<'b, C>
where
    C: Fn(&mut Row) -> Result<T, E>,
    E: std::error::Error + From<Error>,
    //TODO Add size_hint_method
{
    type Item = Result<T, E>;
    fn next(&mut self) -> Option<Self::Item> {
        let stmt = &mut self.row.stmt;
        if self.init_row {
            match &self.status_code {
                Ok(c) if *c == ffi::ResultCode::Row => self.init_row = false,
                _ => return None,
            }
            self.init_row = false;
            stmt.status = Some(ffi::ResultCode::Done);
        } else {
            self.status_code = stmt.step()
        };
        match &self.status_code {
            Ok(c) if *c == ffi::ResultCode::Row => Some((self.factory)(&mut self.row)),
            Ok(_) => {
                self.status_code = Ok(ffi::ResultCode::Done);
                self.init_row = true;
                None
            }
            Err(e) =>  Some(Err(From::from(e.clone()))),
        }
    }
}

impl<'b, C> QueryMapIter<'b, C> {
    pub fn fallible_collect<T, E>(self) -> Result<Vec<T>, E>
    where
        Self: Iterator<Item = Result<T, E>>,
        E: std::error::Error + From<Error>,
//        T: fmt::Debug,
    {
        let mut res = Vec::with_capacity(0);
        for i in self {
            res.push(i?);
        }
        Ok(res)
    }

    pub fn fallible_fn_collect<T, E, TR, R>(self, transform: TR) -> Result<Vec<R>, E>
    where
        Self: Iterator<Item = Result<T, E>>,
        E: std::error::Error + From<Error>,
//        T: fmt::Debug,
        TR: Fn(T) -> R,
    {
        let mut res = Vec::with_capacity(0);
        for i in self {
            res.push(transform(i?));
        }
        Ok(res)
    }
}




#[macro_export]
macro_rules! params {
    ($($p:expr),+) => {
        &[$(Into::<tnsqlite3::Sqlite3Ref>::into($p)),+]
    }
}

#[macro_export]
macro_rules! vec_params {
    ( $p:expr ) => {
        &$p.iter().map(|i| Into::<tnsqlite3::Sqlite3Ref>::into(i) ).collect::<Vec<_>>()
    }
}
