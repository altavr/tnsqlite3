

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use std::os::raw::c_int;
use std::fmt;
use std::error;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));



#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ErrorCode {
    ///* Generic error */
    GeneralError, 
    /// Internal logic error in SQLite
    InternalMalfunction,
    /// Access permission denied
    PermissionDenied,
    /// Callback routine requested an abort
    OperationAborted,
    /// The database file is locked
    DatabaseBusy ,
    /// A table in the database is locked
    DatabaseLocked,
    /// A malloc() failed
    OutOfMemory,
    /// Attempt to write a readonly database
    ReadOnly,
    /// Operation terminated by sqlite3_interrupt()
    OperationInterrupted,
    /// Some kind of disk I/O error occurred
    SystemIOFailure,
    /// The database disk image is malformed
    DatabaseCorrupt,
    /// Unknown opcode in sqlite3_file_control()
    NotFound,
    /// Insertion failed because database is full
    DiskFull,
    /// Unable to open the database file
    CannotOpen,
    /// Database lock protocol error
    FileLockingProtocolFailed,
    /// The database schema changed
    SchemaChanged,
    /// String or BLOB exceeds size limit
    TooBig,
    /// Abort due to constraint violation
    ConstraintViolation,
    /// Data type mismatch
    TypeMismatch,
    /// Library used incorrectly
    APIMisuse,
    /// Uses OS features not supported on host
    NoLargeFileSupport,
    /// Authorization denied
    AuthorizationForStatementDenied,
    /// 2nd parameter to sqlite3_bind out of range
    ParameterOutOfRange,
    /// File opened that is not a database file
    NotADatabase,
   /// Notifications from sqlite3_log()
    Notification,
    ///Warnings from sqlite3_log() 
    WarningFromLog ,
    FunctionMisuse,

    Unknown,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Error {
    pub code: ErrorCode,
    pub extended_code: c_int,
    msg: Option<String>,
}


impl Error {
    pub fn new(result_code: c_int, msg: Option<String>) -> Error {
        let code = match result_code & 0xff {
            
            SQLITE_ERROR => ErrorCode::GeneralError,
            SQLITE_INTERNAL => ErrorCode::InternalMalfunction,
            SQLITE_PERM => ErrorCode::PermissionDenied,
            SQLITE_ABORT => ErrorCode::OperationAborted,
            SQLITE_BUSY => ErrorCode::DatabaseBusy,
            SQLITE_LOCKED => ErrorCode::DatabaseLocked,
            SQLITE_NOMEM => ErrorCode::OutOfMemory,
            SQLITE_READONLY => ErrorCode::ReadOnly,
            SQLITE_INTERRUPT => ErrorCode::OperationInterrupted,
            SQLITE_IOERR => ErrorCode::SystemIOFailure,
            SQLITE_CORRUPT => ErrorCode::DatabaseCorrupt,
            SQLITE_NOTFOUND => ErrorCode::NotFound,
            SQLITE_FULL => ErrorCode::DiskFull,
            SQLITE_CANTOPEN => ErrorCode::CannotOpen,
            SQLITE_PROTOCOL => ErrorCode::FileLockingProtocolFailed,
            SQLITE_SCHEMA => ErrorCode::SchemaChanged,
            SQLITE_TOOBIG => ErrorCode::TooBig,
            SQLITE_CONSTRAINT => ErrorCode::ConstraintViolation,
            SQLITE_MISMATCH => ErrorCode::TypeMismatch,
            SQLITE_MISUSE => ErrorCode::APIMisuse,
            SQLITE_NOLFS => ErrorCode::NoLargeFileSupport,
            SQLITE_AUTH => ErrorCode::AuthorizationForStatementDenied,
            SQLITE_RANGE => ErrorCode::ParameterOutOfRange,
            SQLITE_NOTADB => ErrorCode::NotADatabase,
            SQLITE_NOTICE => ErrorCode::Notification,
            SQLITE_WARNING => ErrorCode::WarningFromLog ,
            SQLITE_FUNCTION => ErrorCode::FunctionMisuse ,
            _ => ErrorCode::Unknown,
        };

        Error {
            code,
             extended_code: result_code,
             msg
        }
    }
}
// 
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Error code {:?}",
            self.code
        )
    }
}


impl error::Error for Error { }

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ResultCode {
    Done,
    Row,
}

impl ResultCode {
    pub fn new(rc: i32) -> ResultCode {
        match rc {
        SQLITE_DONE => ResultCode::Done,
        SQLITE_ROW => ResultCode::Row,
        _ => panic!("Wrong REsultCode")
}

}
}

