extern crate proc_macro;

use proc_macro2::{Span, TokenStream};
use quote::{quote, quote_spanned};
use syn::parse;
use syn::spanned::Spanned;
use syn::{
    Attribute, Data, DeriveInput, Fields,  Ident,  Lit,
    LitStr,  Type,
};

#[proc_macro_derive(Mapping, attributes(mapping_ignore, mapped_length))]
pub fn derive_mapping(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let fields = add_fields(&input.data);

    //        println!("______________{:?}", &fields);

    let expanded = quote! {
        impl Mapping for #name {
            type Target = #name;
            fn mapping(row: &mut tnsqlite3::Row) -> std::result::Result<Self::Target, tnsqlite3::Error> {
               Ok(  #name {
                    #fields
                })

            }
        }
    };
    //    let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}

fn add_fields(data: &Data) -> TokenStream {
    let mut num = 0;
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields.named.iter().map(|f| {
                    let name = &f.ident;
                    let ty = match &f.ty {
                        Type::Path(x) => x.path.segments.last(),
                        _ => panic!(),
                    };
                    let tname = &ty.unwrap().ident;
                    if !f.attrs.iter().any(|a| a.path.is_ident("mapping_ignore")) {
                        let field = quote_spanned! {f.span()=> #name: #tname::mapped(row, #num)?};
                        if let Some(x) = get_mapped_length_var(&f.attrs) {
                            num += x;
                        } else {
                            num += 1;
                        }
                        field
                    } else {
                        quote_spanned! {f.span()=> let #name = #tname::default()}
                    }
                });

                quote! {
                    #(#recurse, )*
                }
            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}



fn get_mapped_length_var(attrs: &Vec<Attribute>) -> Option<i32> {
    for attr in attrs {
        if attr.path.is_ident("mapped_length") {
            match attr.parse_meta().unwrap() {
                syn::Meta::NameValue(x) => {
                    match x.lit {
                        Lit::Int(s) => return Some(s.base10_parse().expect("mapped_length not integer")),
                        _ =>  panic!("mapped_length not integer"),
                    }
                },
                _ => return None,
            }
        }
    }
    None
}



#[proc_macro_derive(Mapped, attributes(mapping_ignore))]
pub fn derive_mapped(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let fields = add_fields_for_mapped(&input.data);

    //        println!("______________{:?}", &fields);

    let expanded = quote! {
        impl Mapped<#name> for #name {
            fn mapped(row: &mut tnsqlite3::Row, column: i32) -> std::result::Result<Self, tnsqlite3::Error> {
               Ok(  #name {
                    #fields
                })

            }
        }
    };
    //    let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}

fn add_fields_for_mapped(data: &Data) -> TokenStream {
    let mut num = -1;
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields.named.iter().map(|f| {
                    let name = &f.ident;
                    let ty = match &f.ty {
                        Type::Path(x) => x.path.segments.last(),
                        _ => panic!(),
                    };
                    let tname = &ty.unwrap().ident;
                    if !f.attrs.iter().any(|a| a.path.is_ident("mapping_ignore")) {
                        num += 1;
                        if tname == "Option" {
                            quote_spanned! {f.span()=> #name: row.get(column + #num)? }
                        } else {
                            quote_spanned! {f.span()=> #name: row.get(column + #num)?
                            .ok_or_else(|| tnsqlite3::Error::InnerError(
                                tnsqlite3::InnerError::new(tnsqlite3::IRR::UnexpectNull(column + #num))) )? }
                        }
                    } else {
                        quote_spanned! {f.span()=> let #name =  #tname::default()}
                    }
                });

//                let recurse_names = fields.named.iter().map(|f| {
//                    let name = &f.ident;
//                    quote_spanned! {f.span()=> #name }
//                });

                quote! {
                    #(#recurse, )*
                }

            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}



#[proc_macro_derive(Insert, attributes(no_insert_column, table))]
pub fn derive_insert(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let table = get_table_var(&input.attrs).expect("Not define attibute \"table\"");
    let col_vec = add_insert(&input.data);
    let columns = col_vec.join(", ");
    let stmt = format!("INSERT INTO {} ({}) VALUES ({})", table, columns,

        gen_placeholders(col_vec.len() ));
        let lit = LitStr::new( &stmt, Span::call_site() );
    //    println!("______________{:?}", &vec);

    let expanded = quote! {
        impl Insert for #name {
            fn stmt() -> &'static str
            {
                #lit
            }

        }
    };
    //        let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}

fn gen_placeholders(n: usize) -> String {
    ["?"]
        .iter()
        .cycle()
        .take(n)
        .copied()
        .collect::<Vec<&str>>()
        .join(",")
}

fn add_insert(data: &Data) -> Vec<String> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                 fields
                    .named
                    .iter()
                    .filter(|f| !f.attrs.iter().any(|a| a.path.is_ident("no_insert_column")))
                    .map(|f|
                        f.ident.as_ref().unwrap().to_string()

                    ).collect()


            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn get_table_var(attrs: &Vec<Attribute>) -> Option<TokenStream> {
    for attr in attrs {
        if attr.path.is_ident("table") {
            match attr.parse_meta().unwrap() {
                syn::Meta::NameValue(x) => {
                    let span = Span::call_site();
                    match x.lit {
                        Lit::Str(s) => {
                            let name = Ident::new(&s.value(), span);
                            return Some(quote!(#name));
                        }
                        _ => panic!(),
                    }
                }
                _ => return None,
            }
        }
    }
    None
}

#[proc_macro_derive(Values, attributes(no_value))]
pub fn derive_values(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let vec = values(&input.data);
    //    println!("______________{:?}", &vec);

    let expanded = quote! {
        impl Values for #name {
            fn values(&self) -> Vec<Sqlite3Ref>
            {
                #vec
            }

        }
    };
    //        let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}


fn values(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields
                    .named
                    .iter()
                    .filter(|f| !f.attrs.iter().any(|a| a.path.is_ident("no_value")))
                    .map(|f| {
                        let name = &f.ident;
                        quote_spanned! {f.span()=>
                            (&self.#name).into()
                        }
                    });

                quote! {
                    vec![#(#recurse, )*]
                }
            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

#[proc_macro_derive(Identify, attributes(row_id))]
pub fn derive_identify(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let row_id_field_name = row_id_field(&input.data).expect("Not define attibute \"row_id\"");

    let expanded = quote! {
        impl tnsqlite3::Identify for #name
         {
            fn identify(&self) -> i64 {
                self.#row_id_field_name
            }
        }
    };
    //    println!("______________{:?}", row_id_field_name);

    proc_macro::TokenStream::from(expanded)
}

fn row_id_field(data: &Data) -> Option<TokenStream> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                for f in fields.named.iter() {
                    if f.attrs.iter().any(|a| a.path.is_ident("row_id")) {
                        let i = &f.ident;
                        return Some(quote!(#i));
                    }
                }
            }
            _ => panic!("Need field attribute \"row_id\""),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }

    None
}
