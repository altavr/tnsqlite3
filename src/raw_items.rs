//#![allow(unused)]

use super::{Error, InnerError, IRR};
use sqlite3_sys1 as ffi;
use sqlite3_sys1::*;
use std::borrow::ToOwned;
use std::ffi::{CStr, CString};
use std::os::raw::*;
use std::{str, thread_local, cmp};
use std::cell::RefCell;
use std::{fmt, mem, ptr, slice};
// use bitflags::bitflags;
use std::time::{UNIX_EPOCH, Duration, SystemTime};
use chrono;

thread_local!(static BIN_OR_TMP: RefCell<Vec<u8>> = RefCell::new(Vec::new()));

//unsafe impl Send for Sqlite3Handler {}


//#[derive(Eq, Ord, PartialOrd, PartialEq)]
//pub struct Sqlite3Handler(*const ffi::sqlite3);

//impl Sqlite3Handler {
//    pub fn compare(&self, h :*const ffi::sqlite3) -> bool {
//        if self.0 == h {true} else {false}
//    }
//}

bitflags! {
    pub struct OpenFlags: i32 {
        const READONLY = 0x00000001;
        const READWRITE = 0x00000002;
        const CREATE = 0x00000004;
        const URI = 0x00000040;
        const MEMORY = 0x00000080;
        const NOMUTEX = 0x00008000;
        const FULLMUTEX = 0x00010000;
        const SHAREDCACHE = 0x00020000;
        const PRIVATECACHE = 0x00040000;
        const WAL = 0x00080000;
    }
}


macro_rules! result_from_rc_msg {
    ($res:expr, $rc:expr, $msg:expr, $ref_code:expr  ) => {
        if $rc == $ref_code {
            Ok($res)
        } else {
            Err(ffi::Error::new($rc, $msg))
        }
    };
    ($res:expr, $rc:expr, $msg:expr, $ref_code0:expr, $ref_code1:expr) => {
        if $rc == $ref_code0 || $rc == $ref_code1 {
            Ok($res)
        } else {
            Err(ffi::Error::new($rc, $msg))
        }
    };
}


macro_rules! result_from_rc {
    ($res:expr, $rc:expr, $ref_code:expr ) => {
        if $rc == $ref_code {
            Ok($res)
        } else {
            Err(ffi::Error::new($rc, None))
        }
    };
    ($res:expr, $rc:expr, $ref_code0:expr, $ref_code1:expr) => {
        if $rc == $ref_code0 || $rc == $ref_code1 {
            Ok($res)
        } else {
            Err(ffi::Error::new($rc, None))
        }
    };
}

#[derive(Debug, PartialOrd, PartialEq, Clone, Copy)]
pub(crate) enum Sqlite3Type {
    NULL,
    INTEGER,
    DOUBLE,
    BLOB,
    TEXT,
}

#[derive(Debug)]
pub(crate) struct RawConnection {
    conn: *mut ffi::sqlite3,
}

impl RawConnection {
    pub(crate) fn new(uri: &str, flags: OpenFlags) -> Result<RawConnection, Error> {
        unsafe {
            let u = CString::new(uri)?;

            let mut db_handler: *mut sqlite3 = mem::uninitialized();
            let rc = sqlite3_open_v2(u.as_ptr(), &mut db_handler, flags.bits, ptr::null());
            Ok(result_from_rc!(
                RawConnection { conn: db_handler },
                rc,
                ffi::SQLITE_OK
            )?)
        }
    }
//    pub(crate) fn handler(&self) -> Sqlite3Handler {
//        Sqlite3Handler(self.conn)
//    }

    fn close(&mut self) -> Result<(), ffi::Error> {
        unsafe {
            let rc = sqlite3_close_v2(self.conn);
            result_from_rc!((), rc, ffi::SQLITE_OK)
        }
    }

    fn get_sqlite3(&self) -> *mut ffi::sqlite3 {
        self.conn
    }

    pub(crate) fn prepare(&mut self, stmt_str: &str) -> Result<RawStatement, ffi::Error> {
        unsafe {
            let stmt_str = CString::new(stmt_str).unwrap();
            let len = stmt_str.as_bytes().len() as c_int;
            let mut stmt: *mut ffi::sqlite3_stmt = mem::uninitialized();
            let rc = sqlite3_prepare_v2(
                self.conn,
                stmt_str.as_ptr(),
                len,
                &mut stmt,
                ptr::null_mut(),
            );
            let msg = self.errmsg();
            result_from_rc_msg!(RawStatement::new(stmt), rc, Some(msg), ffi::SQLITE_OK)
        }
    }

    fn errmsg(&mut self) -> String {
        unsafe {
            let p = ffi::sqlite3_errmsg(self.conn);
            CStr::from_ptr(p).to_str().unwrap().to_owned()
        }
    }

    pub(crate) fn last_insert_rowid(&self) -> i64 {
        unsafe { sqlite3_last_insert_rowid(self.conn) }
    }

    pub(crate) fn autocommit(&mut self) -> bool {
        unsafe {
           sqlite3_get_autocommit(self.conn) == 0    }
    }

    pub(crate) fn create_function(
        &mut self,
        name: &str,
        nargs: i32,
        func: extern "C" fn(*mut ffi::sqlite3_context, c_int, *mut *mut ffi::sqlite3_value),
        flag: FunctionFlag,
        app: *mut c_void
    ) -> Result<(), ffi::Error> {
        let name = CString::new(name).unwrap();

      let flag =  match flag {
          FunctionFlag::Deterministic => ffi::SQLITE_DETERMINISTIC | ffi::SQLITE_UTF8 ,
          FunctionFlag::NonFlag => ffi::SQLITE_UTF8,
        };

        if let Some(ctx) =  unsafe { (app as *const Context).as_ref() } {
            dbg!(ctx);
            println!("address - {:p}", ctx);
        }

        let rc = unsafe {
            ffi::sqlite3_create_function(
                self.conn,
                name.as_ptr(),
                nargs,
                flag,
                 app,
                Some(func),
                None,
                None,
            )
        };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }

    pub(crate) fn create_aggregate_function(
        &mut self,
        name: &str,
        nargs: i32,
        step_func: extern "C" fn(*mut ffi::sqlite3_context, c_int, *mut *mut ffi::sqlite3_value),
        final_func: extern "C" fn(*mut ffi::sqlite3_context),
        flag: FunctionFlag,
        app: *mut c_void
    ) -> Result<(), ffi::Error> {
        let name = CString::new(name).unwrap();

      let flag =  match flag {
          FunctionFlag::Deterministic => ffi::SQLITE_DETERMINISTIC | ffi::SQLITE_UTF8 ,
          FunctionFlag::NonFlag => ffi::SQLITE_UTF8,
        };

        if let Some(ctx) =  unsafe { (app as *const Context).as_ref() } {
            dbg!(ctx);
            println!("address - {:p}", ctx);
        }

        let rc = unsafe {
            ffi::sqlite3_create_function(
                self.conn,
                name.as_ptr(),
                nargs,
                flag,
                 app,
                None,
               Some( step_func),
               Some( final_func),
            )
        };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }


}

#[derive(Debug)]
pub struct Context {
    pub(crate) person_id: i64,
    pub(crate) perm: i64,
}

pub  enum FunctionFlag {
    NonFlag,
    Deterministic,

}

impl Drop for RawConnection {
    fn drop(&mut self) {
//        dbg!(" CLOSEEEEEEEEE connection");
        let r = self.close();
        match r {
            Ok(_) => (),
            Err(e) => panic!("Error close db - {0}", e),
        }
    }
}

#[derive(Clone)]
pub(crate) struct RawStatement {
    stmt: *mut ffi::sqlite3_stmt,
}

impl RawStatement {
    fn new(stmt: *mut ffi::sqlite3_stmt) -> RawStatement {
        RawStatement { stmt }
    }



    pub(crate) fn column_count(&self) -> i32 {
        unsafe { sqlite3_column_count(self.stmt) }
    }

    pub(crate) fn bind_parameter_count(&mut self) -> i32 {
        unsafe { sqlite3_bind_parameter_count(self.stmt) }
    }

    pub(crate) fn reset(&mut self) -> Result<(), ffi::Error> {
        let rc = unsafe { sqlite3_reset(self.stmt) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }


    pub(crate) fn step(&mut self) -> Result<ffi::ResultCode, ffi::Error> {
        unsafe {
            let rc = sqlite3_step(self.stmt);
            result_from_rc!(
                ffi::ResultCode::new(rc),
                rc,
                ffi::SQLITE_ROW,
                ffi::SQLITE_DONE
            )
        }
    }

    pub(crate) fn finilize(&mut self) -> Result<(), ffi::Error> {
        let rc = unsafe { sqlite3_finalize(self.stmt) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }

    pub(crate) fn column_type(&self, i_col: i32) -> Result<Sqlite3Type, InnerError> {
        let tc = unsafe { sqlite3_column_type(self.stmt, i_col) };
        match tc {
            ffi::SQLITE_INTEGER => Ok(Sqlite3Type::INTEGER),
            ffi::SQLITE_FLOAT => Ok(Sqlite3Type::DOUBLE),
            ffi::SQLITE_BLOB => Ok(Sqlite3Type::BLOB),
            ffi::SQLITE_NULL => Ok(Sqlite3Type::NULL),
            ffi::SQLITE3_TEXT => Ok(Sqlite3Type::TEXT),
            _ => Err(InnerError::new(IRR::TransVal)),
        }
    }

    pub(crate) fn column_bytes(&self, i_col: i32) -> i32 {
        unsafe { sqlite3_column_bytes(self.stmt, i_col) }
    }

    //The leftmost column of the result set has the index 0
    pub(crate) fn column_int64(& self, i_col: i32) -> Result<i64, ffi::Error> {
        Ok(unsafe { sqlite3_column_int64(self.stmt, i_col) })
    }

    pub(crate) fn column_double(& self, i_col: i32) -> Result<f64, ffi::Error> {
        Ok(unsafe { sqlite3_column_double(self.stmt, i_col) })
    }

    pub(crate) fn column_text(
        &self,
        i_col: i32,
        conn: &  RefCell<RawConnection>
    ) -> Result<Option<String>, Error> {
        let result = unsafe { sqlite3_column_text(self.stmt, i_col) };
        if result.is_null() {
            let rc = unsafe { sqlite3_extended_errcode( conn.borrow_mut().get_sqlite3()) };
            return Ok(result_from_rc!(None, rc, ffi::SQLITE_OK )?);
        }
        let len = self.column_bytes(i_col) as usize;
        let array: Vec<_> = unsafe { ToOwned::to_owned(slice::from_raw_parts(result, len)) };
        Ok(Some(String::from_utf8(array)?))
    }

    pub(crate) fn column_blob(
        & self,
        i_col: i32,
        conn: &  RefCell<RawConnection>,
    ) -> Result<Option<Vec<u8>>, Error> {
        let result = unsafe { sqlite3_column_blob(self.stmt, i_col) } as *const u8;
        if result.is_null() {
            let rc = unsafe { sqlite3_extended_errcode( conn.borrow_mut().get_sqlite3() ) };
            return Ok(result_from_rc!(None, rc, ffi::SQLITE_OK)?);
        }
        let len = self.column_bytes(i_col) as usize;
        Ok(Some(unsafe {
            ToOwned::to_owned(slice::from_raw_parts(result, len))
        }))
    }

    //The leftmost SQL parameter has an index of 1
    pub(crate) fn bind_int64(&mut self, i_col: i32, value: i64) -> Result<(), ffi::Error> {
        let rc = unsafe { sqlite3_bind_int64(self.stmt, i_col, value) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }
    pub(crate) fn bind_double(&mut self, i_col: i32, value: f64) -> Result<(), ffi::Error> {
        let rc = unsafe { sqlite3_bind_double(self.stmt, i_col, value) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }
    pub(crate) fn bind_text(&mut self, i_col: i32, value: &str) -> Result<(), ffi::Error> {
        let (s, len) = sqlite3_str(value);
        let rc = unsafe { sqlite3_bind_text(self.stmt, i_col, s, len, sqlite_transient()) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }
    pub(crate) fn bind_blob(&mut self, i_col: i32, value: &[u8]) -> Result<(), ffi::Error> {
        let rc = unsafe {
            sqlite3_bind_blob(
                self.stmt,
                i_col,
                value as *const [u8] as *const c_void,
                value.len() as i32,
                sqlite_transient(),
            )
        };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }
    pub(crate) fn bind_null(&mut self, i_col: i32) -> Result<(), ffi::Error> {
        let rc = unsafe { sqlite3_bind_null(self.stmt, i_col) };
        result_from_rc!((), rc, ffi::SQLITE_OK)
    }

    fn expanded_sql(& self) -> Result<&str, str::Utf8Error> {
        let s = unsafe { CStr::from_ptr(sqlite3_sql(self.stmt)) };
        s.to_str()
    }
}

impl fmt::Debug for RawStatement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "SQL: {:?}",  self.expanded_sql().unwrap_or("SQL: NLL"))
    }
}

impl Drop for RawStatement {
    fn drop(&mut self) {
//         dbg!(&self);
//        dbg!(" CLOSEEEEEEEEE stat");
        let _ = self.finilize();
    }
}

unsafe fn from_sqlite3_str(p: *const c_char, len: u32) -> String {

   let v = slice::from_raw_parts(p as *const u8, len as usize).to_owned();
    String::from_utf8_unchecked(v)

}

fn sqlite3_str(s: &str) -> (*const c_char, c_int) {
    (s.as_ptr() as *const c_char, s.as_bytes().len() as c_int)
}

fn sqlite_transient() -> ffi::sqlite3_destructor_type {
    Some(unsafe { mem::transmute(-1isize) })
}

pub(crate) extern "C" fn fmtdatetime(
    ctx: *mut ffi::sqlite3_context,
    nargs: c_int,
    values: *mut *mut ffi::sqlite3_value,
) {
    unsafe {
        if nargs != 1 {
//            let msg = b"Function cdatetime take 1 argument\x00";
//            sqlite3_result_error(ctx, c_str.as_ptr() as *const c_char, -1);
            sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
            return;
        }
        if sqlite3_value_type(*values) != ffi::SQLITE_INTEGER {
//            let msg =
//                b"Argument of cdatetime must be integer\x00";
//            sqlite3_result_error(ctx, msg.as_ptr() as *const c_char, (msg.len() - 1) as i32);
            sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
            return;
        }
        let timestamp = ffi::sqlite3_value_int64(*values);
        let d = UNIX_EPOCH + Duration::from_secs(timestamp as u64);
        let date_time = chrono::DateTime::<chrono::offset::Local>::from(d);
        let fmt_datetime = date_time.format("%Y-%m-%d %H:%M:%S").to_string();
        let c_str = CString::new(fmt_datetime)
        .expect("datetype format is not null terminated string ");

        sqlite3_result_text(ctx, c_str.as_ptr(),
            (c_str.to_bytes().len() - 1) as i32, sqlite_transient())
    }
}


pub(crate) extern "C" fn cdatetime(
    ctx: *mut ffi::sqlite3_context,
    _nargs: c_int,
    values: *mut *mut ffi::sqlite3_value,
) {
    unsafe{
        let ts =  SystemTime::now().duration_since(UNIX_EPOCH)
        .expect("SystemTime before UNIX EPOCH!")
        .as_secs() as i64;

        sqlite3_result_int64(ctx, ts);
    }
}



pub(crate) extern "C" fn zeroblob(
    ctx: *mut ffi::sqlite3_context,
    _nargs: c_int,
    values: *mut *mut ffi::sqlite3_value,
) {
    unsafe{
        dbg!("");
        if sqlite3_value_type(*values) != ffi::SQLITE_INTEGER {
            dbg!(sqlite3_value_type(*values) );
            sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
            return;
        }
        dbg!("");
        let len = sqlite3_value_int64(*values) as usize;
        if len == 0 {
            sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
            return;
        }
        dbg!("");
        let arr = vec![0u8; len];
sqlite3_result_blob(
    ctx,
    arr.as_ptr() as *const c_void,
    len as c_int,
    sqlite_transient(),
);
    }
}





struct AggContext {
    len: usize,
    data: *mut u8,
}

pub(crate) extern "C" fn bin_or_vec_step(
    ctx: *mut ffi::sqlite3_context,
    nargs: c_int,
    values: *mut *mut ffi::sqlite3_value,
) {
    unsafe {
        const HEADER_LEN: usize = mem::size_of::<AggContext>();
        dbg!("");
        let value = *values;
        if sqlite3_value_type(value) != ffi::SQLITE_BLOB {
            if sqlite3_value_type(value) != ffi::SQLITE_NULL {
                dbg!("");
                sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
                return;
            }
        };

        let len = sqlite3_value_int64(*values.wrapping_offset(1));
        let len_vec = sqlite3_value_bytes(value) as i64;

        if len < len_vec {
            dbg!(len, len_vec);
            sqlite3_result_error_code(ctx, ffi::SQLITE_FUNCTION);
            return;
        }
        dbg!(len, sqlite3_value_int64(*values.wrapping_offset(1)));

        let agg_ctx_ptr =
            sqlite3_aggregate_context(ctx, HEADER_LEN as c_int + len as c_int);
        if agg_ctx_ptr.is_null() {
            sqlite3_result_error_nomem(ctx);
        }
        dbg!(HEADER_LEN);
        let agg_ctx = mem::transmute::<*mut c_void, *mut AggContext>(agg_ctx_ptr);
        let result = sqlite3_value_blob(value) as *mut u8;
        dbg!("");

        if (*agg_ctx).len == 0 {
            (*agg_ctx).len = len as usize;
            (*agg_ctx).data = (agg_ctx_ptr as *mut u8).wrapping_offset(HEADER_LEN as isize);
//                        ptr::write_bytes((*agg_ctx).data, 0, len as usize);
            ptr::copy(result,  (*agg_ctx).data, len_vec as usize);
        } else {
            dbg!("");
            for i in 0..cmp::min(len_vec, len) {
                *(*agg_ctx).data.wrapping_offset(i as isize) |= *result.wrapping_offset(i as isize);
            }
        }
        dbg!("");

    }
}



pub(crate) extern "C" fn bin_or_vec_final(ctx: *mut ffi::sqlite3_context) {
    unsafe {


        let agg_ctx_ptr = sqlite3_aggregate_context(ctx, 0) as *mut u8;
        if agg_ctx_ptr.is_null() {
            sqlite3_result_null(ctx);
            dbg!("NULL");
            return;
        }
        let agg_ctx = mem::transmute::<*mut u8, *mut AggContext>(agg_ctx_ptr);
        if (*agg_ctx).len > 0 {
            dbg!("NOT NULL");
        sqlite3_result_blob(
            ctx,
            (*agg_ctx).data as *const c_void,
            (*agg_ctx).len as c_int,
            sqlite_transient(),
        );
        } else { sqlite3_result_null(ctx); }
    }
}


#[cfg(test)]
mod tests {

    use super::{RawStatement, RawConnection, OpenFlags};

    #[test]
    fn error_test() {
        let mut conn = unsafe {
            RawConnection::new(
                ":memory:",
                OpenFlags::READWRITE | OpenFlags::CREATE | OpenFlags::URI,
            ).unwrap()
        };
        let table = "CREATE TABLE task (
        id INTEGER not null,
        name  TEXT not null)";
        let r_stmt = conn.prepare(table).unwrap();


    }
}
