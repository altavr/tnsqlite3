//use tn_proc::{Mapping, Mapped};
//use tntransition;
//use tnsqlite3;
use tnsqlite3_proc::{Mapping, Mapped};

pub mod tnsqlite3 {

    #[derive(Debug)]
    pub enum Error {
        InnerError(InnerError),
    }

    #[derive(Debug)]
    pub struct InnerError {}

    impl InnerError {
        pub fn new(e: IRR) -> Self {
            InnerError {}
        }
    }

    #[derive(Debug)]
    pub enum IRR {
        UnexpectNull(i32),
    }

    pub struct Row {
        count: i32,
    }

    impl Row {
        pub fn new() -> Self {
            Row { count: -1 }
        }

    pub    fn get<T>(&mut self, idx: i32) -> Result<Option<T>, Error>
        where
            T: From<String>,
        {
            match idx {
                0 => Ok(Some(From::from("1".to_string()))),
                1 => Ok(Some(From::from("2".to_string()))),
                2 => Ok(Some(From::from("3".to_string()))),
                3 => Ok(Some(From::from("4".to_string()))),
                _ => unreachable!(),
            }
        }

        fn get_inc<T>(&mut self) -> Result<Option<T>, Error>
        where
            T: From<String>,
        {
            self.count += 1;
            self.get(self.count)
        }
    }

pub trait Mapping {
    type Target;
    fn mapping(row: &mut Row) -> Result<Self::Target, Error>;
}

pub trait Mapped<T> {
    fn mapped(row: &mut Row, column: i32) -> Result<T, Error>;
}


//    impl Mapped for String {
//        fn mapped(row: &mut Row, column: i32) -> Result<String, Error> {
//            row.get(column)?
//                .ok_or_else(|| Error::InnerError(InnerError::new(IRR::MappingError(column))))
//        }

//    }


}
use tnsqlite3::{Mapped, Mapping, Row};

macro_rules! impl_mapped {
    ($target:ty) => {
        impl Mapped<$target> for $target {
            fn mapped(row: &mut Row, column: i32) -> Result<Self, tnsqlite3::Error> {

                row.get(column)?
                    .ok_or_else(|| tnsqlite3::Error::InnerError(
                    tnsqlite3::InnerError::new(tnsqlite3::IRR::UnexpectNull(column))))
            }
        }
    };
}

macro_rules! impl_mapped_option {
    ($target:ty) => {
        impl Mapped<Option<$target>> for Option<$target> {
            fn mapped(row: &mut Row, column: i32) -> Result<Self, tnsqlite3::Error> {
               Ok( row.get(column)?)
            }
        }
    };
}


impl_mapped!(String);
impl_mapped_option!(String);

//impl Mapping for AA {
//    type Target = AA;
//    fn mapping(row: &mut Row) -> Result<Self::Target, tnsqlite3::Error> {
//        Ok(AA {
//            id: String::mapped(row, 0)?,
//            a: String::mapped(row, 1)?,
//        })
//    }
//}



#[derive(Debug,  Mapped, Mapping)]
struct AA {
    id: String,
    a: String,
}

#[derive(Debug, Mapping)]
struct BB {

    b: String,
    #[mapped_length=2]
    a: AA,
    c: Option<String>,
}

#[test]
fn derive_mapping_test() {
    let mut row = Row::new();
    let a = AA::mapping(&mut row).unwrap();
    assert_eq!(a.a, "2".to_string());
    println!("{:?}", a);
    let mut row = Row::new();
    let b = BB::mapping(&mut row).unwrap();
    println!("{:?}", b);
    assert_eq!(b.a.a, "3".to_string());
    assert_eq!(b.b, "1".to_string());
    assert_eq!(b.c, Some("4".to_string()));
//    println!("{:?}", b);
}
